# How to config Spacemacs

# How do you add an external directory to the repository?

I want to add an external directory to an existing repository.

External Dir: /home/some/directory

Working Dir: /htdocs/.git

If I attempt the following command from the /htdocs dir:

git add /home/some/directory
I get an error: fatal: '/home/some/directory' is outside repository

If I need to do something like that I would normally move that external file or directory into my git repo and symlink it's original location to the new one.

```console

mv /home/some/directory /htdocs/directory
ln -s /htdocs/directory /home/some/
git add ./directory
```


I use this technique when I am developing a plug-in for an application that I want to keep under version control but have to store in a specific location.