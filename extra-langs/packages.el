(setq extra-langs-packages
  '(
    arduino-mode
    julia-mode
    matlab-mode
    qml-mode
    scad-mode
    stan-mode
    thrift
    ;; removed from MELPA (https://github.com/syl20bnr/spacemacs/issues/9795)
    ;; TODO re-enable this mode when it is added back to MELPA
    ;; wolfram-mode
    ))

(defun extra-langs/init-arduino-mode ()
  (use-package arduino-mode :defer t))

(defun extra-langs/init-scad-mode ()
  (use-package scad-mode :defer t))

(defun extra-langs/init-qml-mode ()
  (use-package qml-mode :defer t :mode "\\.qml\\'"))

(defun extra-langs/init-julia-mode ()
  (use-package julia-mode :defer t))
;;--------------------
;; Matlab:
(defun extra-langs/init-matlab-mode ()
  (use-package matlab-mode
    :ensure t
    :mode ("\\.m$" . matlab-mode)
    :bind (:map matlab-shell-mode-map
                ("C-c C-c" . term-interrupt-subjob))
    :init
    (setq matlab-shell-command "/Applications/MATLAB_R2016b.app/bin/matlab"
          matlab-indent-function t)
    (eval-after-load 'matlab
      '(add-to-list 'matlab-shell-command-switches "-nosplash"))))

(defun sk/matlab-shell-here ()
  "opens up a new matlab shell in the directory associated with the current buffer's file."
  (interactive)
  (split-window-right)
  (other-window 1)
  (matlab-shell))
(global-set-key "\C-c="  'sk/matlab-shell-here)
;;-------------------

(defun extra-langs/init-stan-mode ()
  (use-package stan-mode :defer t))

(defun extra-langs/init-thrift ()
  (use-package thrift :defer t))

;; .m files are not associated because conflict with more common Objective-C and
;; MATLAB/Octave, manually invoke for .m files.
(defun extra-langs/init-wolfram-mode ()
  (use-package wolfram-mode
    :defer t
    :interpreter "\\(Wolfram\\|Mathematica\\)Script\\( -script\\)?"
    :mode "\\.wl\\'"))
